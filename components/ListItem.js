// ListItem.js

import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

class ListItem extends Component {
  handleClick = () => {
    alert('')
  }
  
  render() {
    return (
      <>
        <View style = { styles.listItem } onClick = { () => handleClick() }>
          <Text>{ this.props.placeName }</Text>
        </View>
      </>
    );
  }

}

const styles = StyleSheet.create({
  listItem: {
    width: '100%',
    padding: 10,
    marginBottom: 10,
    backgroundColor: '#eee'
  }
});

export default ListItem;