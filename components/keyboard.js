import React from 'react';
import {View, StyleSheet, I18nManager} from 'react-native';
import Play from '../icons/Play';
import Pause from '../icons/Pause';
import SkipForward from '../icons/Skipforward';
import SkipBack from '../icons/Skipback';
import Prev from '../icons/Prev';
import Next from '../icons/Next';

const Keyboard = ({onPlay, onPause, playStatus, onPrev, onNext, onSkipForward, onSkipBackward}) => {
  return (
    <View style={styles.wrapper}>
      <View style={[styles.buttons, styles.buttons_prev]}>
        <Prev onPrev={onPrev} />
      </View>
      {/* <View style={[styles.buttons, styles.buttons_skip_previous]}>
        <SkipBack onSkipBack={onSkipBackward} />
      </View> */}
      <View style={[styles.buttons, styles.buttons_play]}>
        {playStatus ? <Pause onPause={onPause} /> : <Play onPlay={onPlay} />}
      </View>
      {/* <View style={[styles.buttons, styles.buttons_skip_forward]}>
        <SkipForward onSkipForward={onSkipForward} />
      </View> */}
      <View style={[styles.buttons, styles.buttons_next]}>
        <Next onNext={onNext} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    height: 100,
    width: 400,
    flexDirection: I18nManager.isRTL ? 'row-reverse' : 'row',
  },
  third: {
    flex: 10,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  buttons: {
    flex: 2,
    justifyContent: 'center',
    height: '100%'
  },
});

export default Keyboard;
