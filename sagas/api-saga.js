import { takeEvery, call, put } from "redux-saga/effects";
import { API_ERRORED, DATA_LOADED, DATA_REQUESTED, Ocp_Apim_Subscription_Key } from "../actions/types";
import * as rssParser from 'react-native-rss-parser';

export default function* watcherSaga() {
    yield takeEvery(DATA_REQUESTED, workerSaga);
}

function* workerSaga(args) {
    try {
        const payload = yield call(getDataSaga, args);
        yield put({ type: args.args.callbackFunction, payload });
    } catch (e) {
        yield put({ type: API_ERRORED, payload: e });
    }
}

function getDataSaga(args) {
    return fetch(args.args.baseURL, {})
    .then((response) =>  response.text())
    .then((responseData) => rssParser.parse(responseData))
    .then((rss) => rss);
}