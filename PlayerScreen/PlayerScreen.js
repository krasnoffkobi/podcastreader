import React, { Component } from 'react';
import { View, Text, Image, AppState, Platform } from 'react-native';
import { connect } from 'react-redux';
import styles from '../style/styles';
import TrackPlayer from 'react-native-track-player';
import Keyboard from '../components/keyboard';
import { ProgressBar } from '@react-native-community/progress-bar-android';
import Slider from '@react-native-community/slider';


class PlayerScreen extends Component {
    static navigationOptions = {
        title: 'Player - Kobi Krasnoff',
    };

    myProgressTimer = null;
    myTimer = null;

    state = {
        isPlaying: false,
        appState: AppState.currentState,
        progress: 0.0,
        duration: 0.0,
        durationFormat: '00:00',
        durationProgress: '00:00',
        currentTrack: {
            artist: "",
            artwork: "https://previews.123rf.com/images/alekseyvanin/alekseyvanin1705/alekseyvanin170501394/77840540-feed-icon-vector-rss-solid-logo-illustration-colorful-pictogram-isolated-on-white.jpg",
            id: "",
            title: "",
            url: ""
        }
    }
    
    componentDidMount() {
        TrackPlayer.setupPlayer().then(async () => {

            TrackPlayer.updateOptions({
                stopWithApp: true,
                capabilities: [
                  TrackPlayer.CAPABILITY_PLAY,
                  TrackPlayer.CAPABILITY_PAUSE,
                  TrackPlayer.CAPABILITY_SKIP_TO_NEXT,
                  TrackPlayer.CAPABILITY_SKIP_TO_PREVIOUS,
                ],
                compactCapabilities: [
                  TrackPlayer.CAPABILITY_PLAY,
                  TrackPlayer.CAPABILITY_PAUSE,
                  TrackPlayer.CAPABILITY_SKIP_TO_NEXT,
                  TrackPlayer.CAPABILITY_SKIP_TO_PREVIOUS,
                ]
            });
        
            // Adds a track to the queue
            this.props.TrackPlayerList.forEach(async (el) => {
                await TrackPlayer.add(el);
            });
        
            // Starts playing it
            TrackPlayer.play();
            this.setState({
                isPlaying: true
            });
        });

        AppState.addEventListener('change', this._handleAppStateChange);
        if (Platform.OS === 'android') {
            AppState.addEventListener('focus', this._handleFocusState);
        }
        
        this.myProgressTimer = setInterval( () => { 
            this.getCurrentProgress();
        }, 100);

        this.myTimer = setInterval( () => { 
            this.updateTimer();
        }, 1000);

        this.onTrackChange = TrackPlayer.addEventListener(
            'playback-track-changed',
            async data => {
              const track = await TrackPlayer.getTrack(data.nextTrack);
              this.setState({currentTrack: track});
              console.log(this.state.currentTrack)
            },
        );
    }

    async getCurrentProgress() {
        const duration = await TrackPlayer.getDuration();
        const position = await TrackPlayer.getPosition();
        this.setState({
            progress: duration > 0 ? position / duration : 0.0,
            duration: duration
        });
    }

    async updateTimer() {
        const duration = await TrackPlayer.getDuration();
        const position = await TrackPlayer.getPosition();
        this.setState({durationFormat: this.formatTime(duration), durationProgress: this.formatTime(position)});
    }

    formatTime(seconds) {
        const totalMinutes = Math.floor(seconds / 60);
        const totalSeconds = Math.floor(seconds - totalMinutes * 60);
        return totalMinutes.toString().padStart(2, '0') + ':' + totalSeconds.toString().padStart(2, '0')
    }

    componentWillUnmount() {
        this.stopPlayer();
        AppState.removeEventListener('change', this._handleAppStateChange);
        if (Platform.OS === 'android') {
            AppState.removeEventListener('focus', this._handleFocusState);
        }

        clearInterval(this.myProgressTimer);
        this.onTrackChange.remove();
    }

    _handleAppStateChange = (nextAppState) => {
        if (
          this.state.appState.match(/inactive|background/) &&
          nextAppState === 'active'
        ) {
            this._setPlayState();
        }
        this.setState({appState: nextAppState});
    };

    _handleFocusState = (nextAppState) => {
        this._setPlayState();
    }

    _setPlayState() {
        TrackPlayer.getState().then(res => {
            this.setState({
                isPlaying: res == TrackPlayer.STATE_PLAYING ? true : false
            });
        });
    }

    onPlay = () => {
        this.startPlayer();
    };

    onPause = () => {
        this.pausePlayer();
    };

    onPrev = () => {
        this.prevTrack();
    };

    onNext = () => {
        this.nextTrack();
    };

    async startPlayer() {
        try {
            await TrackPlayer.play();
            this.setState({
                isPlaying: true
            });
        } catch (_) {}
    }

    async pausePlayer() {
        try {
            await TrackPlayer.pause();
            this.setState({
                isPlaying: false
            });
        } catch (_) {}
    }

    async stopPlayer() {
        try {
            await TrackPlayer.stop();
        } catch (_) {}
    }

    async prevTrack() {
        try {
          await TrackPlayer.skipToPrevious();
        } catch (_) {}
    }

    async nextTrack() {
        try {
          await TrackPlayer.skipToNext();
        } catch (_) {}
    }

    async getMyState() {
        let state = false;
        try {
            state = await TrackPlayer.getState();
        } catch (_) {}
        return state;
    }

    onSlidingComplete = async (progressValue) => {
        try {
            console.log('progressValue', progressValue.progressValue * this.state.duration)
            await TrackPlayer.seekTo(progressValue.progressValue * this.state.duration);
        } catch (_) {}
    }  
    
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.textContainer}><Text style={styles.artist}>{this.state.currentTrack ? this.state.currentTrack.title : ''}</Text></View>
                <View style={styles.imageContainer}>
                    {this.state.currentTrack ? 
                        <Image
                            style={{width: 200, height: 200}} 
                            source={{
                                uri: this.state.currentTrack.artwork,
                            }}>
                        </Image>
                    :
                        <Image
                            style={{width: 200, height: 200}} 
                            source={require('../img/rss.png')}>
                        </Image>
                    }
                </View>
                <View style={styles.buttonsContainer}>
                <Keyboard
                    onPlay={() => this.onPlay()}
                    onPause={() => this.onPause()}
                    playStatus={this.state.isPlaying}
                    onPrev={() => this.onPrev()}
                    onNext={() => this.onNext()}
                    onSkipForward={() => this.onSkipForward()}
                    onSkipBackward={() => this.onSkipBackward()}
                    style={styles.third}
                    />
                </View>
                <View style={styles.textContainer}>
                    {Platform.OS === 'android' ?
                        <View>
                            <Slider
                                style={{height: 40}}
                                minimumValue={0}
                                maximumValue={1}
                                minimumTrackTintColor="#999999"
                                maximumTrackTintColor="#999999"
                                value={this.state.progress}
                                onSlidingComplete = {(progressValue) => this.onSlidingComplete({progressValue})}
                            />
                        </View>
                    :
                        null
                    }
                </View>
                <View style={styles.buttonsContainer}>
                    <Text>{this.state.durationProgress} / {this.state.durationFormat}</Text>
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        TrackPlayerList: ownProps.route.params.TrackPlayerList
    }
}

const mapDispatchToProps = dispatch => {
    return {
     
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PlayerScreen)