import React from 'react';
import { Button, StyleSheet, View, Image, Text, TextInput, Alert } from 'react-native';
import { connect } from 'react-redux';
import { getDataSaga } from '../actions/rest';
import { RSS_URL, DEVICE_LIST_LODED, BASE_URL } from '../actions/types';
import TrackPlayer from 'react-native-track-player';

class SetFeedScreen extends React.Component {
    static navigationOptions = {
        title: 'Welcome',
    };

    state = {
        OriginalXMLResponse: {},
        TrackPlayerList: [],
        rssFeedValue: 'https://petapodcast.libsyn.com/rss'
      }

    args = {
        str: 'sdfsdfsdf',
        baseURL: RSS_URL,
        callbackFunction: DEVICE_LIST_LODED
    }

    componentDidMount() {
       
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.OriginalXMLResponse !== prevProps.OriginalXMLResponse) {
          this.setState({OriginalXMLResponse: this.props.OriginalXMLResponse});

          console.log(this.props.OriginalXMLResponse)
          
          if (this.props.OriginalXMLResponse.length > 0) {
            this.setState({TrackPlayerList: this.props.OriginalXMLResponse});
            this.props.TrackPlayerList = this.props.OriginalXMLResponse;
            this.updateTrackPlayer();
            this.props.navigation.navigate('DrawerPages', {screen: 'Player', params: {TrackPlayerList: this.props.OriginalXMLResponse}})
          }
        }
        
        
        if (this.props.errorCode !== prevProps.errorCode) {
            alert('Error: ' + this.props.errorCode)
        }
    }

    async updateTrackPlayer() {
      try {
        await TrackPlayer.pause();
        await TrackPlayer.reset();
        this.setState({
            isPlaying: false
        });

        // Adds a track to the queue
        this.props.OriginalXMLResponse.forEach(async (el) => {
          await TrackPlayer.add(el);
        });
    
        // Starts playing it
        TrackPlayer.play();
        this.setState({
            isPlaying: true
        });
      } catch (_) {}
    }

    updatedataSaga() {
      this.args = {
        str: 'sdfsdfsdf',
        baseURL: this.state.rssFeedValue,
        callbackFunction: DEVICE_LIST_LODED
      }
      console.log(this.args)
      this.props.getDataSaga(this.args);  
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <View style={styles.subContainer}><Text>Enter new RSS feed</Text></View>
                <View style={styles.subContainer}>
                  <TextInput style={styles.inputbox} value={this.state.rssFeedValue} onChangeText={(rssFeedValue) => this.setState({rssFeedValue})} /></View>
                <View style={styles.subContainer}>
                  <Button
                    title="Press me"
                    onPress={() => this.updatedataSaga()}
                /></View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    subContainer: {
      width: '100%',
      paddingLeft: 10,
      paddingRight: 10,
      marginTop: 5,
      marginBottom: 5
    }, 
    inputbox: {
      height: 40, 
      borderColor: 'gray', 
      borderWidth: 1, 
      width: '100%',
      borderRadius: 5,
      borderColor: '#003399'
    }
  });

  const mapStateToProps = state => {
    return {
      OriginalXMLResponse: state.rests.OriginalXMLResponse,
      TrackPlayerList: state.rests.TrackPlayerList,
      errorCode: state.rests.errorCode
    }
  }
  
  const mapDispatchToProps = dispatch => {
    return {
      getDataSaga: args => dispatch(getDataSaga(args))
    }
  }

  export default connect(mapStateToProps, mapDispatchToProps)(SetFeedScreen)