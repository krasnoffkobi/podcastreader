export const ADD_PLACE = 'ADD_PLACE';
export const ADD_ARTICLE = "ADD_ARTICLE";
export const FOUND_BAD_WORD = "FOUND_BAD_WORD";
export const DATA_LOADED = "DATA_LOADED";
export const DATA_REQUESTED = "DATA_REQUESTED";
export const API_ERRORED = "API_ERRORED";

export const DEVICE_LIST_LODED = "DEVICE_LIST_LODED";
export const SEARCH_RESULTS_LOADED = "SEARCH_RESULTS_LOADED";

export const RSS_URL = "https://feed.podbean.com/veganfriendly/feed.xml";