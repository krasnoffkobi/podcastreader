import React from 'react';
import styles from '../style/styles';
import {TouchableOpacity, Image} from 'react-native';

const Prev = ({onPrev}) => {
  return (
    <TouchableOpacity
      onPress={onPrev}
      style={[styles.innerTouchableOpacity, styles.next_prev]}>
      <Image
          style={{width: 50, height: 50}} 
          source={require('../img/skip_previous.png')}>
      </Image>
    </TouchableOpacity>
  );
};

export default Prev;
