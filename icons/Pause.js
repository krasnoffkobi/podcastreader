import React from 'react';
import styles from '../style/styles';
import {TouchableOpacity, Image} from 'react-native';

const Pause = ({onPause}) => {
  return (
    <TouchableOpacity onPress={onPause} style={styles.innerTouchableOpacity}>
      <Image
          style={{width: 60, height: 60}} 
          source={require('../img/pause.png')}>
      </Image>
    </TouchableOpacity>
  );
};

export default Pause;
