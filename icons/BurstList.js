import React from 'react';
import styles from '../style/styles';
import {TouchableOpacity} from 'react-native';
import Svg, {Path, G, Rect} from 'react-native-svg';

function Burstlist() {
  return (
    <TouchableOpacity
      onPress={this.playHandler}
      style={styles.innerTouchableOpacity}>
      <Svg viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
        <G fill-rule="evenodd">
          <Rect height="2.909" rx=".5" width="16" y="7.273" />
          <Rect height="2.909" rx=".5" width="16" y="13.091" />
          <Rect height="2.909" rx=".5" width="8.727" x="7.273" y="1.455" />
          <Path d="M4.924 3.356l-4.2 2.1A.5.5 0 0 1 0 5.01V.81A.5.5 0 0 1 .724.362l4.2 2.1a.5.5 0 0 1 0 .894z" />
        </G>
      </Svg>
    </TouchableOpacity>
  );
}

export default Burstlist;
