import React from 'react';
import styles from '../style/styles';
import {TouchableOpacity} from 'react-native';
import Svg, {Path, Mask, Polygon, G} from 'react-native-svg';

function Share() {
  return (
    <TouchableOpacity
      onPress={this.playHandler}
      style={styles.innerTouchableOpacity}>
      <Svg viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
        <Mask height="16" maskUnits="userSpaceOnUse" width="16" x="0" y="0">
          <Polygon
            points="0 0 16 0 16 16 0 16 0 0"
            style="fill:#fff;fill-rule:evenodd"
          />
        </Mask>
        <G style="mask:url(#a)">
          <Path
            d="M5.65,8c0,.06,0,.11,0,.16L11.36,11a2.78,2.78,0,0,1,1.82-.68,2.83,2.83,0,1,1-2.83,2.82c0-.05,0-.1,0-.16L4.64,10.15a2.85,2.85,0,0,1-1.82.67,2.83,2.83,0,0,1,0-5.65,2.86,2.86,0,0,1,1.82.68L10.37,3s0-.1,0-.16a2.83,2.83,0,1,1,2.83,2.83A2.78,2.78,0,0,1,11.36,5L5.63,7.84s0,.1,0,.16"
            style="fill-rule:evenodd"
          />
        </G>
      </Svg>
    </TouchableOpacity>
  );
}

export default Share;
