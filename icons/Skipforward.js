import React from 'react';
import styles from '../style/styles';
import {TouchableOpacity} from 'react-native';
import Svg, {G, Path, Text} from 'react-native-svg';

const SkipForward = ({onSkipForward}) => {
  return (
    <TouchableOpacity onPress={onSkipForward} style={styles.innerTouchableOpacity}>
      <Svg viewBox="0 0 29.7 31.4" height="100%" width="100%">
        <G style="isolation:isolate">
          <Text
            font-family="Avenir-Medium, Avenir"
            font-size="10"
            font-weight="500"
            style="isolation:isolate"
            transform="matrix(1, -0.02, 0.02, 1, 9.26, 19.83)">
            30
          </Text>
        </G>
        <Path
          d="M27.9,11.6a13.9,13.9,0,0,1-8,18,14.1,14.1,0,0,1-18.2-8A14.1,14.1,0,0,1,9.8,3.5,13.5,13.5,0,0,1,18.2,3"
          fill="none"
          stroke="#000"
          stroke-linecap="round"
          stroke-width="1.6"
        />
        <Path
          d="M21.6,2.4,17.8.1a.5.5,0,0,0-.7.1V5.1a.6.6,0,0,0,.6.5h.2l3.8-2.2a.8.8,0,0,0,.2-.8Z"
          fill="#0d0d0d"
          fill-rule="evenodd"
        />
      </Svg>
    </TouchableOpacity>
  );
};

export default SkipForward;
