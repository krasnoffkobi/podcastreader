import React from 'react';
import styles from '../style/styles';
import {TouchableOpacity} from 'react-native';
import Svg, {G, Path, Text} from 'react-native-svg';

const SkipBack = ({onSkipBack}) => {
  return (
    <TouchableOpacity onPress={onSkipBack} style={styles.innerTouchableOpacity}>
      <Svg viewBox="0 0 29.7 31.8" height="100%" width="100%">
        <G style="isolation:isolate">
          <Text
            font-family="Avenir-Medium, Avenir"
            font-size="10"
            font-weight="500"
            style="isolation:isolate"
            transform="matrix(1, 0.02, -0.02, 1, 8.93, 20.03)">
            30
          </Text>
        </G>
        <Path
          d="M1.7,12A14,14,0,1,0,19.9,3.9a13.8,13.8,0,0,0-8.5-.5"
          fill="none"
          stroke="#000"
          stroke-linecap="round"
          stroke-width="1.6"
        />
        <Path
          d="M8.5,2.8,12.1.1a.6.6,0,0,1,.8.1c.1.1.1.2.1.4V5.9a.5.5,0,0,1-.5.5h-.4L8.5,3.7a.7.7,0,0,1-.2-.8Z"
          fill="#0d0d0d"
          fill-rule="evenodd"
        />
      </Svg>
    </TouchableOpacity>
  );
};

export default SkipBack;
