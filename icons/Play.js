import React from 'react';
import styles from '../style/styles';
import {TouchableOpacity, Image} from 'react-native';

const Play = ({onPlay}) => {
  return (
    <TouchableOpacity onPress={onPlay} style={styles.innerTouchableOpacity}>
      <Image
          style={{width: 60, height: 60}} 
          source={require('../img/play.png')}>
      </Image>
    </TouchableOpacity>
  );
};

export default Play;
