import React from 'react';
import styles from '../style/styles';
import {TouchableOpacity} from 'react-native';
import Svg, {Path} from 'react-native-svg';

function Heart() {
  return (
    <TouchableOpacity
      onPress={this.playHandler}
      style={styles.innerTouchableOpacity}>
      <Svg viewBox="0 0 20.4 20" xmlns="http://www.w3.org/2000/svg">
        <Path
          d="M20.4,6c-.4-3.5-2.7-6-5.6-6a5.6,5.6,0,0,0-4.6,2.9A5.2,5.2,0,0,0,5.6,0C2.7,0,.4,2.5,0,6A8.2,8.2,0,0,0,.2,8.2a10.4,10.4,0,0,0,2.8,5L10.2,20l7.2-6.8a10,10,0,0,0,2.8-5A8.2,8.2,0,0,0,20.4,6"
          fill-rule="evenodd"
        />
      </Svg>
    </TouchableOpacity>
  );
}

export default Heart;
