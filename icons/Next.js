import React from 'react';
import styles from '../style/styles';
import {TouchableOpacity, Image} from 'react-native';

const Next = ({onNext}) => {
  return (
    <TouchableOpacity
      onPress={onNext}
      style={[styles.innerTouchableOpacity, styles.next_prev]}>
      <Image
          style={{width: 50, height: 50}} 
          source={require('../img/skip_next.png')}>
      </Image>
    </TouchableOpacity>
  );
};

export default Next;
